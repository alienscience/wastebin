module gitlab.com/alienscience/wastebin/server

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/jackc/pgx/v4 v4.15.0
)
