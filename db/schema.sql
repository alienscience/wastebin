
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- To insert:
--    INSERT INTO paste DEFAULT VALUES RETURNING paste_id;
CREATE TABLE paste (
    paste_id uuid DEFAULT uuid_generate_v4(),
    created_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

