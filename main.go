package main

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/jackc/pgx/v4"
)

const (
	insertPasteSQL = "INSERT INTO paste DEFAULT VALUES RETURNING paste_id"
)

var dbConn *pgx.Conn
var dataPath string

//go:embed html/*
var htmlEmbed embed.FS

func dbConnect(databaseUrl string) (*pgx.Conn, error) {
	config, err := pgx.ParseConfig(databaseUrl)
	if err != nil {
		return nil, err
	}
	return pgx.ConnectConfig(context.Background(), config)
}

func insertPaste(context context.Context, dbConn *pgx.Conn) (string, error) {
	var pasteId *string
	err := dbConn.QueryRow(context, insertPasteSQL).Scan(&pasteId)
	return *pasteId, err
}

func writePaste(path string, paste []byte) error {
	return os.WriteFile(path, paste, 0444)
}

func main() {
	address := os.Getenv("ADDRESS")
	databaseUrl := os.Getenv("DATABASE_URL")
	dataPath = os.Getenv("DATA")
	var err error
	dbConn, err = dbConnect(databaseUrl)
	if err != nil {
		log.Fatal("Cannot connect to db:", err)
	}
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	htmlSub, _ := fs.Sub(htmlEmbed, "html")
	htmlFS := http.FileServer(http.FS(htmlSub))
	pasteFS := http.FileServer(http.Dir("./data"))
	r.Get("/*", func(h http.ResponseWriter, r *http.Request) {
		htmlFS.ServeHTTP(h, r)
	})
	r.Get("/paste/*", func(h http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/paste/", pasteFS).ServeHTTP(h, r)
	})
	r.Post("/", addPaste)
	http.ListenAndServe(address, r)
}

func addPaste(w http.ResponseWriter, r *http.Request) {
	id, err := insertPaste(r.Context(), dbConn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	pth := path.Join(dataPath, id)
	content := r.FormValue("content")
	err = writePaste(pth, []byte(content))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	dest := path.Join("/paste", id)
	http.Redirect(w, r, dest, http.StatusSeeOther)
	msg := fmt.Sprint("Inserted paste ", id)
	w.Write([]byte(msg))
}
